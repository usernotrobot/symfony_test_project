Task Application
================

Application based on Symfony3 and AngularJS.

Requirements
============
* PHP 7
* MySQL 5.7
* Composer
* PHPUnit
* npm, gulp, bower, sass
