'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('js', function() {
    return gulp.src([
        './web/assets/vendor/jquery/dist/jquery.js',
        './web/assets/vendor/angular/angular.js',
        './web/assets/vendor/angular-resource/angular-resource.js',
        './web/assets/vendor/angular-ui-router/release/angular-ui-router.js',
        './src/AppBundle/Resources/public/js/app.js',
        './src/AppBundle/Resources/public/js/controllers.js',
        './src/AppBundle/Resources/public/js/services.js',
        './src/AppBundle/Resources/public/js/directives.js',
        './src/AppBundle/Resources/public/js/filters.js'])
        .pipe(sourcemaps.init({largeFile: true}))
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write('./', {
            sourceMappingURL: function(file) {
                return file.relative + '.map';
            }
        }))
        .pipe(gulp.dest('./web/assets/js'));
});

gulp.task('css', function() {
    return gulp.src([
        './src/AppBundle/Resources/public/sass/app.scss'])
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(concat('app.css'))
        .pipe(gulp.dest('./web/assets/css'));
});

gulp.task('fonts', function() {
    return gulp
        .src(['./web/assets/vendor/bootstrap-sass/assets/fonts/bootstrap/*'])
        .pipe(gulp.dest('./web/assets/fonts/bootstrap'));
});

gulp.task('default', ['js', 'css', 'fonts']);

gulp.task('watch', function() {
    gulp.watch('src/*/Resources/public/*/**', ['default']);
});
