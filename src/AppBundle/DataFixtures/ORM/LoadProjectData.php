<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Task;
use AppBundle\Entity\Board;
use AppBundle\Entity\Project;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadProjectData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->getProjects() as $projectData) {
            $project = new Project();
            $project->setTitle($projectData['title']);
            $project->setSlug($projectData['slug']);
            $project->setSort($projectData['sort']);
            $manager->persist($project);

            foreach ($this->getBoards() as $boardData) {
                $board = new Board();
                $board->setTitle($boardData['title']);
                $board->setSort($boardData['sort']);
                $board->setProject($project);
                $manager->persist($board);

                foreach ($boardData['tasks'] as $taskData) {
                    $task = new Task();
                    $task->setTitle($taskData['title']);
                    $task->setPriority($taskData['priority']);
                    $task->setBoard($board);
                    $manager->persist($task);
                }
            }
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 30;
    }

    /**
     * @return array
     */
    private function getProjects()
    {
        return [
                [
                'title'  => 'PgPlabs',
                'slug'   => 'pgplabs',
                'sort'   => 10,
                'boards' => $this->getBoards()
            ],
                [
                'title'  => 'Gmail',
                'slug'   => 'gmail',
                'sort'   => 30,
                'boards' => $this->getBoards()
            ],
                [
                'title'  => 'Google Search',
                'slug'   => 'google-search',
                'sort'   => 20,
                'boards' => $this->getBoards()
            ]
        ];
    }

    /**
     * @return array
     */
    private function getBoards()
    {
        return [
                [
                'title' => 'TODO',
                'sort'  => 1,
                'tasks' => $this->getTasks()
            ],
                [
                'title' => 'Closed',
                'sort'  => 5,
                'tasks' => $this->getTasks()
            ],
                [
                'title' => 'In progress',
                'sort'  => 2,
                'tasks' => $this->getTasks()
            ],
                [
                'title' => 'Tested',
                'sort'  => 4,
                'tasks' => $this->getTasks()
            ],
                [
                'title' => 'Done',
                'sort'  => 3,
                'tasks' => $this->getTasks()
            ]
        ];
    }

    /**
     * @return array
     */
    private function getTasks()
    {
        static $i = 1;
        $tasks = [];

        foreach (range(1, mt_rand(1, 10)) as $index) {
            $tasks[] = [
                'title'    => sprintf('Task #%s', $i),
                'priority' => mt_rand($index, 100)
            ];

            $i++;
        }

        return $tasks;
    }

}
