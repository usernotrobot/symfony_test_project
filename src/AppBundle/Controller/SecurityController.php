<?php

namespace AppBundle\Controller;

use AppBundle\Form\LoginType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{

    /**
     * @Configuration\Route("/login", name="app.security.login")
     */
    public function loginAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app.default');
        }

        $helper = $this->get('security.authentication_utils');

        $form = $this->createForm(LoginType::class, [
            'username' => $helper->getLastUsername()
        ]);

        return $this->render('AppBundle:Security:login.html.twig', [
                'form'  => $form->createView(),
                'error' => $helper->getLastAuthenticationError()
        ]);
    }

    /**
     * @Configuration\Route("/logout", name="app.security.logout")
     */
    public function logoutAction()
    {
        return $this->redirectToRoute('app.default');
    }

    /**
     * @Configuration\Route("/403.html", name="app.security.denied")
     */
    public function deniedAction()
    {
        return $this->render('AppBundle:Security:denied.html.twig');
    }

}
