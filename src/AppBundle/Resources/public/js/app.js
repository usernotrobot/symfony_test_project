angular.module('pmApp', ['ui.router', 'ngResource', 'pmApp.controllers', 'pmApp.services']);

angular.module('pmApp')
    .config(function($stateProvider, $interpolateProvider) {
        $stateProvider
            .state('projects', {
                url: '/projects',
                templateUrl: 'projects.html',
                controller: 'ProjectsController'
            })
            .state('project', {
                url: '/projects/:id',
                templateUrl: 'project.html',
                controller: 'ProjectController'
            })
            .state('task', {
                url: '/projects/:project/tasks/:id',
                templateUrl: 'task.html',
                controller: 'TaskController'
            })
            .state('task.create', {
                url: '/tasks/create',
                templateUrl: 'task.create.html'
            })
            .state('task.update', {
                url: '/tasks/:id/update',
                templateUrl: 'task.update.html'
            });

        // $interpolateProvider.startSymbol('[[');
        // $interpolateProvider.endSymbol(']]');
        // $locationProvider.html5Mode(true);
    })
    .run(function($state) {
        $state.go('projects');
    });
