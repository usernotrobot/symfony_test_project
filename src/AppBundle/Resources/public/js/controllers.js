angular
    .module('pmApp.controllers', [])
    .controller('ProjectsController', function($scope, Project) {
        $scope.projects = Project.query();
    })
    .controller('ProjectController', function($scope, $stateParams, Project) {
        $scope.project = Project.get({id: $stateParams.id});
    })
    .controller('TaskController', function($scope, $stateParams, Task) {
        $scope.task = Task.get({id: $stateParams.id, project: $stateParams.project});
    });
