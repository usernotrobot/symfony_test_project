angular
    .module('pmApp.services', [])
    .factory('Project', function($resource) {
        return $resource('/api/projects/:id', {id: '@id'}, {
            update: {
                method: 'PUT'
            }
        });
    })
    .factory('Task', function($resource) {
        return $resource('/api/projects/:project/tasks/:id', {id: '@id', project: '@project'}, {
            update: {
                method: 'PUT'
            }
        });
    });
