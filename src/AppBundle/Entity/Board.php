<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="app_boards", indexes={
 *      @ORM\Index(name="project_idx", columns={"project_id"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BoardRepository")
 */
class Board implements \Serializable, \JsonSerializable
{

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $color;

    /**
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $sort;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="boards")
     * @ORM\JoinColumn(
     *      name="project_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE",
     *      nullable=false
     * )
     *
     * @var Project
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity="Task", mappedBy="board")
     *
     * @var ArrayCollection|Task[]
     */
    private $tasks;

    /**
     * Creating a new entity.
     */
    public function __construct()
    {
        $this->tasks = new ArrayCollection();

        $this->setSort(0);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     *
     * @return self
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return int
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param int $color
     *
     * @return self
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @param Task $task
     *
     * @return self
     */
    public function addTask(Task $task)
    {
        $this->tasks->add($task);

        return $this;
    }

    /**
     * @param Task $task
     *
     * @return self
     */
    public function removeTask(Task $task)
    {
        $this->tasks->removeElement($task);

        return $this;
    }

    /**
     * @return ArrayCollection|Task[]
     */
    public function getTasks()
    {
        return $this->tasks->toArray();
    }

    /**
     * @param Project $project
     *
     * @return self
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->title,
            $this->color,
            $this->sort,
            $this->project,
            $this->tasks
        ]);
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->title,
            $this->color,
            $this->sort,
            $this->project,
            $this->tasks
            ) = unserialize($serialized);
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id'    => $this->id,
            'title' => $this->title,
            'color' => $this->color,
            'sort'  => $this->sort,
            'tasks' => $this->tasks
        ];
    }

}
