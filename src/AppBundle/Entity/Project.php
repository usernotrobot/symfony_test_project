<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="app_projects")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 */
class Project implements \Serializable, \JsonSerializable
{

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $sort;

    /**
     * @ORM\OneToMany(targetEntity="Board", mappedBy="project")
     *
     * @var ArrayCollection|Board[]
     */
    private $boards;

    /**
     * Creating a new entity.
     */
    public function __construct()
    {
        $this->boards = new ArrayCollection();

        $this->setSort(0);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param int $slug
     *
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return int
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param int $sort
     *
     * @return self
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param Board $board
     *
     * @return self
     */
    public function addBoard(Board $board)
    {
        $this->boards->add($board);

        return $this;
    }

    /**
     * @param Board $board
     *
     * @return self
     */
    public function removeBoard(Board $board)
    {
        $this->boards->removeElement($board);

        return $this;
    }

    /**
     * @return ArrayCollection|Board[]
     */
    public function getBoards()
    {
        return $this->boards->toArray();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->title,
            $this->slug,
            $this->sort,
            $this->boards
        ]);
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->title,
            $this->slug,
            $this->sort,
            $this->boards
            ) = unserialize($serialized);
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id'     => $this->id,
            'title'  => $this->title,
            'slug'   => $this->slug,
            'sort'   => $this->sort,
            'boards' => $this->boards
        ];
    }

}
