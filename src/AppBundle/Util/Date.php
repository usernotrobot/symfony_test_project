<?php

namespace AppBundle\Util;

use DateTime;

class Date
{

    /**
     * @param DateTime $date
     * @param string   $format
     *
     * @return string|null
     */
    public static function format(DateTime $date = null, $format = 'd.m.Y H:i:s')
    {
        if (null !== $date) {
            return $date->format($format);
        }
    }

}
