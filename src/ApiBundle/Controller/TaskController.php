<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Task;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Configuration\Route("api")
 */
class TaskController extends Controller
{

    /**
     * @Configuration\Route("/tasks")
     * @Configuration\Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tasks = $em->getRepository(Task::class)->findAll();

        return $this->json($tasks);
    }

    /**
     * @Configuration\Route("/projects/{project}/tasks/{id}")
     * @Configuration\Method("GET")
     */
    public function showAction(Task $task)
    {
        return $this->json($task);
    }

}
