<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Project;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Configuration\Route("api")
 */
class ProjectController extends Controller
{

    /**
     * @Configuration\Route("/projects")
     * @Configuration\Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $projects = $em->getRepository(Project::class)->findAll();

        return new JsonResponse($projects);
    }

    /**
     * @Configuration\Route("/projects/{id}")
     * @Configuration\Method("GET")
     */
    public function showAction(Project $project)
    {
        return $this->json($project);
    }

    /**
     * @Configuration\Route("/projects/{id}")
     * @Configuration\Method("PUT")
     */
    public function updateAction(Request $request, Project $project)
    {
        return new JsonResponse([$request->get('title')]);
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $category = $em->find(Category::class, $data['id']);

        if (null === $category) {
            return $this->json(['errors' => ['Category not found']], 422);
        }

        $data = ['name' => $data['name']];
        $form = $this->createForm(CategoryType::class, $category);

        if ($form->submit($data)->isValid()) {
            $em->persist($category);
            $em->flush();

            return $this->json(['success' => true]);
        }

        return $this->json(['errors' => $form->getErrors()], 422);
    }

}
