<?php

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20161205000000 extends AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('app_boards');

        $table->addColumn('id', 'integer', ['autoincrement' => true, 'unsigned' => true]);
        $table->addColumn('project_id', 'integer', ['unsigned' => true]);
        $table->addColumn('title', 'string', ['length' => 255]);
        $table->addColumn('color', 'string', ['length' => 32, 'notnull' => false]);
        $table->addColumn('sort', 'smallint', ['unsigned' => true]);

        $table->setPrimaryKey(['id']);
        $table->addIndex(['project_id'], 'project_idx');
        $table->addForeignKeyConstraint('app_projects', ['project_id'], ['id'], ['onDelete' => 'CASCADE']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('app_boards');
    }

}
